Role Name
=========

A role that installs the OSS version of Sonatype Nexus, <https://www.sonatype.com/nexus/repository-oss>

Role Variables
--------------

The most important variables are listed below:

``` yaml
nexus_behind_nginx: True
nexus_service_port: 8081
nexus_service_bind_host: 127.0.0.1
nexus_major_version: '2'
#nexus_version: '{{ nexus_major_version }}.28.1'
nexus_version: '{{ nexus_major_version }}.14.19'
nexus_release: '01'
nexus_download_url: 'https://download.sonatype.com/nexus/{{ nexus_major_version }}/nexus-{{ nexus_version }}-{{ nexus_release }}-unix.tar.gz'
nexus_install_basedir: /srv/nexus
nexus_webapp_dir: '{{ nexus_install_basedir }}/nexus-oss-webapp'
nexus_data_basedir: /data
nexus_data_dir: '{{ nexus_data_basedir }}/sonatype-work/nexus'
nexus_logdir: '/var/log/nexus'
nexus_accesslog_maxhistory: 20
nexus_wrapper_initmemory: 256
nexus_wrapper_maxmemory: 1024
nexus_wrapper_conf_dir: '{{ nexus_webapp_dir }}/bin/jsw/conf'
nexus_wrapper_logdir: '/var/log/nexus-wrapper'
# MB
nexus_wrapper_diskcache_buffersize: 4096
```

Dependencies
------------

* nginx
* openjdk

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
